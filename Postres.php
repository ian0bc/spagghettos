<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="HomeStyle.css">
</head>
<body>
        <div class = "Bienvenida">
            <div class="Titulo">
                <H1> Postres</H1>
            </div>
        </div>
            
        <div class = "Contenido">
            <br>
            <?php 
                $servidor = "localhost";
                $usuario = "root";
                $password = "";
                $database = "restaurante";

                $conexion = new mysqli($servidor, $usuario, $password, $database);

                if($conexion->connect_error){
                    die("conexión fallida"."<br>");
                }
                else{
                    //echo "<br>conexión exitosa"."<br>";
                }
                //SQL
                $sql = "SELECT * FROM alimentos WHERE id_tipo_alimento = 3";
                $resultado = $conexion->query($sql);
                
                ?> 
                <script>
                    var lista = [];
                    var parsed_data;
                </script>
                <?php

                if($resultado->num_rows>0){
                    $contador = 1;
                    while($fila = $resultado->fetch_assoc()){
                        ?>
                        <div class="cuadrito">
                            <br>
                            <div class="Imagen">
                                <img src="Resources\Imagenes\<?php echo $fila['imagen']?>" height="300" width="450"  >
                            </div>

                            <div class="Informacion"> 
                                <?php 
                                echo $fila['nombre_platillo']."<br>"; 
                                echo "Precio: $".$fila['precio']."<br>";
                                echo $fila['descripcion']."<br>";
                                ?>
                            </div>

                            <div class = "Comprar">
                                <button id="btnComprar<?php echo $contador?>"> Añadir al carrito </button>
                                <script>
                                var btnComprar<?php echo $contador?> = document.getElementById('btnComprar<?php echo $contador?>');

                                btnComprar<?php echo $contador?>.onclick = function(){
                                    
                                    var Postres<?php echo $contador ?> = "<?php echo $fila['nombre_platillo'] ?>";
                                    var PrecioPostres<?php echo $contador ?> = <?php echo $fila['precio'] ?>;
                                    var xhr = new XMLHttpRequest();

                                    var infoJSON = {
                                        'nombre' : Postres<?php echo $contador ?>,
                                        'precio' : PrecioPostres<?php echo $contador ?>
                                    };

                                    lista.push(infoJSON);
                                    parsed_data = JSON.stringify(lista);
                                    sessionStorage.setItem("lista", parsed_data);
                                }
                            </script>
                            </div>
                        </div>

                        <div class="espacio"></div>

                        <?php
                        $contador +=1;
                        }
                    }
                    ?>
        </div>
</body>
</html>